let slideShow;
const slidesIn = document.querySelector(".images-wrapper");
const images = [...slidesIn.querySelectorAll('.image-to-show')];
let index = 0;
function makeSlide() {
    slideShow = setInterval(function() {
        index += 1;
        if (index === images.length) {
            index = 0;
        }
        slidesIn.style.transform = `translate3d(${index * -400}px , 0 , 0)`;
        document.querySelectorAll(".btn").forEach(el => {
            el.classList.remove('d-none')
        });
    }, 2000);
}
makeSlide();
const finishBtn = document.getElementById("finish-btn");
finishBtn.addEventListener("click", function () {
    clearInterval(slideShow);
})
const startBtn = document.getElementById("start-btn");
startBtn.addEventListener("click", function () {
       images.findIndex(makeSlide(index));
})